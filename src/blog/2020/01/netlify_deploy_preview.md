---
title: Netlifyのプレビューデプロイを設定する
lang: ja-JP
layout: Post
published: 2020-01-23 01:50
tags:
  - Netlify
  - gitlab
category:
  - Netlify
  - gitlab
meta:
  - property: og:title
    content: Netlifyのプレビューデプロイを設定する
  - name: description
    content: Netlifyでサイトのプレビューがしたい！
  - property: og:description
    content: Netlifyでサイトのプレビューがしたい！
  - name: keywords
    content:
      - Netlify
      - gitlab
  - property: og:type
    content: article
---

# Netlifyのプレビューデプロイを設定する

## 事の発端

<iframe src="https://mstdn.tsukiyono.0am.jp/@tsukiyonomegumi/103526481120171009/embed" class="mastodon-embed" style="max-width: 100%; border: 0" width="400" allowfullscreen="allowfullscreen"></iframe><script src="https://mstdn.tsukiyono.0am.jp/embed.js" async="async"></script>

というのもいつもサイト（blog）を更新するのには
 1. ブランチを切る
 2. 記事なりページを書く
 3. masterにマージ
 4. gitlabへpush
 5. Netlifyの自動デプロイが発火して本番環境に反映される

という手順を踏んでいました。

この手順ですと、vuepress内蔵のdevコマンドで確認するしかありません。もっとスマートな方法が無いのか・・・  
と気軽にトゥートしたところ[えあい氏](https://stellaria.network/@Eai)からアドバイスをいただいた。_（ありがとうえあい）_

<iframe src="https://stellaria.network/@Eai/103526484687829696/embed" class="mastodon-embed" style="max-width: 100%; border: 0" width="400" allowfullscreen="allowfullscreen"></iframe>
<iframe src="https://stellaria.network/@Eai/103526503317442119/embed" class="mastodon-embed" style="max-width: 100%; border: 0" width="400" allowfullscreen="allowfullscreen"></iframe>

んで、気になったのでちょっと調べてみたら、[ねじわさ氏](https://don.nzws.me/@nzws)が既にgithubでの実行方法を書かれていました。  
[Netlify の Deploy Preview を使ってみる · nzws's 備忘録](https://blog.nzws.me/2019/07/try-netlify-pr-preview/)  
こんなのがあったのか。なるほど。

## Netlifyの設定をする

とのことで設定してみる。  
まずNetlifyのサイト設定のページを開いて`Setting`->`Build & deploy`->`Deploy notifications`へ行く。  
ヘルプへのリンクがあったので少し見てみる。[Deploy notifications | Netlify Docs](https://docs.netlify.com/site-deploys/notifications/#outgoing-webhooks-and-notifications)  
元々はデプロイされたら通知先と連携を目的としている様だが、通知先によってはプレビューのデプロイとかテストビルドをしてくれるらしい。  

### gitlab側でAPIトークンを発行しておく

githubではAPIトークンは不要だが、gitlabの場合はAPIトークンを発行する必要がある。ということで発行する。  
右上のユーザアイコンから`setting`を選択するとユーザ設定のページに飛ぶので、左の`User Setting`から`Access Tokens`を選択。  
`Name`は適当に`Netlify deploy notifications`とかわかりやすい名前に  
`Expires at`はトークンの失効日を指定します。私は適当に100年くらい先の`2120-12-31`と書いておきました。  
`Scopes`には有効にするトークンにチェックします。今回はAPIだけ必要なので`API`にチェック。
![gitlabのAPIトークン設定項目](/img/blog/20200123_netlify_gitlab_api1.jpg)  
::: tip
gitlabのアクセストークンの設定についてはGitlab Docsにヘルプがあります。  
[Personal access tokens | GitLab](https://docs.gitlab.com/ce/user/profile/personal_access_tokens.html)
:::

入力が完了したら`Create personal access token`をクリック。  
`Your new personal access token has been created.`と表示されて、下にトークンが表示されて発行するのでメモします。この表示は1度のみ表示されて、再度確認できないので忘れずにメモしておきます。また、他人へトークンが漏れないように注意してください。
![gitlabのAPIトークン発行後](/img/blog/20200123_netlify_gitlab_api2.jpg)  

### Netlify側の設定

Netlify側に戻ります。
`Deploy notifications`の`Add notification`から`Gitlab merge request comment`をクリック。
![Netlify側の設定1](/img/blog/20200123_netlify_deploy_notification1.jpg)  

`Event to listen for`を`Deploy Preview succeeded`を指定します。`API access token`には先ほどgitlabから取得したトークンを入力します。最後に`Save`をクリックして設定完了です。
![Netlify側の設定2](/img/blog/20200123_netlify_deploy_notification2.jpg)  
::: tip
この設定ではデプロイのプレビュービルドが成功した場合のみ通知されます。  
失敗した場合も通知したい場合は`Event to listen for`で`Deploy Preview faild`を選択した物を追加で作成してください。  
トークンは同じ物を使用しても問題ありません。私は失敗しても通知したいため作成しました。  
:::

## 動作するか確認

すべての設定が終了したので、実際にMR(githubで言うPR)を行ってみます。  
gitlabのサイトからMRを作成するとPipelineのメッセージが作成され・・・
![gitlabの通知確認](/img/blog/20200123_netlify_deploy_test1.jpg)  

ビルドが完了するとステータスがpassedに変更されます。
![gitlabの通知確認2](/img/blog/20200123_netlify_deploy_test2.jpg)  

プレビュー用のURLがトークンを発行したユーザで自動的に書き込まれます。
![gitlabの通知確認3](/img/blog/20200123_netlify_deploy_test3.jpg)

### コミット単位で確認したい場合

先ほどの設定はMRのみでのデプロイでしたが、コミット単位でも実行することが可能です。  
`Deploy notifications`の設定項目にはあらかじめ`Send commit status to GitLab when Deploy Preview starts`、`Send commit status to GitLab when Deploy Preview succeeds`、`Send commit status to GitLab when Deploy Preview fails`が設定されているので、右の`･･･`から`edit nitification`を選択、API tokenにトークンを入力します。  
さらに`Build & deploy`の設定に`Deploy contexts`の項目があるので`Edit setting`をクリックし、`Branch deploys`を`All`に設定しておくとすべてのコミットに対して実行されます。  
![コミット単位の設定](/img/blog/20200123_netlify_deploy_tips1.jpg)
::: tip
ここで`Let me add individual branches`を選択すると、特定のブランチに対してデプロイを発火することができます。たとえば対象ブランチに`develop`ブランチに対して有効にしておけば、別に切ったブランチに対しては反応しません。たとえば`feature`と名付けたブランチをgitlab上にpushしても`feature`に対してデプロイは発火しません。
:::

これで試しにpushを行うとコミットの欄にMRと同様にデプロイの結果が表示されます。Netlifyの場合はわざわざgitlab CIを使用するよりかは簡単だと思います。
![コミット単位で実行するようにした場合のgitlabの表示](/img/blog/20200123_netlify_deploy_tips2.jpg)

## 最後に

これでgitlab上でプレビューすることができるようになりました。わざわざ`yarn dev`とか打たなくてよくなりました。まあリアルタイムプレビューが便利なときもあるので使い方次第ですね。たとえばgitlabのwebUIから編集した場合なんかでもプレビューできるので、プレビュー手段は多数あった方が便利ですね。  

ちょっと話はずれますが、ブログの記事を出先の環境とかでうまいことする方法ですが、[ほた氏](https://mstdn.maud.io/@hota)がブログでPR（ないしMR）を使うことで公開する方法を編み出していました。思わず「なるほど、そう言う方法もあるのか」と感心してしまった。こちらの記事でもDeploy previewsについて少し触れています。  
詳しくは[2019年に読んだコミックを振り返る - ﾏﾂﾀﾞｲｱﾘｰ](https://blog.maud.io/entry/2019/12/25/hota-comic-award-2019/)の`おまけ`を参照してください。  

__Netlifyはいいぞ。__
