---
title: mastodonサーバのSSDを大容量の物に変更しました。（Linuxの大容量SSDへの移行）
lang: ja-JP
layout: Post
published: 2020-01-19 20:41
tags:
  - Mastodon
  - SSD
  - Linux
category:
  - Mastodon
  - Linux
meta:
  - property: og:title
    content: mastodonサーバのSSDを大容量の物に変更しました。
  - property: og:image
    content: https://www.tsukiyono.blue/img/blog/20200119_ssd_old_and_new_og.png
  - name: description
    content: LinuxのSSDからSSDへの移行方法
  - property: og:description
    content: LinuxのSSDからSSDへの移行方法
  - name: keywords
    content:
      - Linux
      - SSD
  - property: og:type
    content: article
---

# mastodonサーバのSSDを大容量の物に変更しました。（Linuxの大容量SSDへの移行）

![新旧SSD](/img/blog/20200119_ssd_old_and_new.png)

個人で構築しているMastodonサーバのHDDがそろそろ限界を迎えつつあるので、より大容量の物に変更しました。  
メンテナンスにてダウンタイムが生じましたが、無事完了することができました。ご協力ありがとうございました。また、それによって生じた連合の配送リトライが発生した事かと思いますが、連合各位にはご迷惑をおかけ致しました。

さて、弊Mastodonサーバ[mstdn.tsukiyono.0am.jp](https://mstdn.tsukiyono.0am.jp)ですが、128GBのSSDを使用していますが、過去データが蓄積されてきたため、[バックアップスクリプト](https://gitlab.com/tsukiyonomegumi/mstdn-backup-script)を走らせるとDisk Fullのため5分ほどサーバからStatus 500を返す状態となっていました。  
これによって、毎日5時頃に5分ほどアクセスできなくなっていました。まあ、今のとこをお一人様状態なので問題になるのはわたしだけですが、今後データが増えることを考えるとあまり良くない状態でしたので、SSD余ってるしこの際思い切って128GB SSDから500GB SSDへ移行ししました。  
![黄色マークは全部バックアップによるダウンタイム](/img/blog/20200119_downtime.png)

## 移行前のSSDと移行先のSSD

移行前のSSDですが、これがまたとてもふるいSSDを使用していました。というのも、元々メインPCから取り出したSSDでプレクスターのサイトを見たところ、なんとEOLが2012年・・・  
[PLEXTOR M3](https://www.goplextor.com/jp/Product/Detail/M3) この128GBを使用していました。  

移行先のSSDはツクモのセール（なんでセールしてたかは忘れた）で購入した[WD BLUE 500GB](https://www.cfd.co.jp/product/ssd/wds500g2b0a/)。    
なんか税込みで5278円とかいう恐ろしい安さで売られていたやつ。価格.comの底値でも6808円だったので本当に恐ろしいほど安かった・・・  
中のNANDはSanDisk、実質東芝メモリでコントローラはMarvell使ってるしなかなか。
 そういえば、東芝メモリは2019年の10月に社名を「キオクシア」に変更した上に、[2020年4月からキオクシアブランドでコンシューマ向けSSDを販売する予定](https://pc.watch.impress.co.jp/docs/news/event/1228512.html)だそうですね。楽しみです。   

## 移行その１ - SSDのデータをコピーする

あらかじめ下調べしていたのですが、まあ多分ddコマンドで中身全部コピーしてやればそのまま起動するはず・・・と言う感じでえいやしました。GUIでGpartedとかつかうとか散見されますが、そんな必要な無いです。（GUI環境入れてないしGUIでやるのめんどくせえというのが本音）  
まずサーバからSSDを引っこ抜き、USB経由でraspberry piにSSDを二つブッ刺しました。らずぴっぴ4はUSB 3.0が2つ生えているのでこういうときに便利ですね。  
今回はraspberry piが良い感じに余っていたので使いましたが、移行元のSSDに予期せぬ書き込みが生じなければ良いので、UbuntuのLive CD使えば問題ないと思います。
そして
``` sh
fdisk -l
```
を実行して、移行元のと移行先のSSDのデバイス名を探します。ログを取り忘れたので詳細は割愛します。今回は移行元が`/dev/sdb`、移行先が`/dev/sda`に割り当てられていました。  
んで、ddコマンドでデータを丸ごとコピーします。
``` sh
sudo dd id=/dev/sda of=/dev/sdb
```
を実行。もし途中経過が見たい場合はもう一つシェルを起動して（byobuとかtmuxとかなら新しいタブ作ったり、sshならもう一つセッションを増やす）
``` sh
sudo watch -n 5 "pkill -USR1 dd"
```
とすれば途中経過が見られます。`-n`のあとの数字でポーリングする秒数を好きにいじれます。  
余談なんですが、pkill送る以外でddの途中経過を表示する方法ってないんですかね・・・まあ確かに _"do one thing, and do it well"(1つのことだけをうまくやる)_ という設計思想を考えれば妥当なんですけど・・・  

コピー（dd）が終わるとシェルが戻ってきます。128GBのコピーで3時間47分かかりました。そしてパーティションの確認
``` sh
fdisk -l
Disk /dev/sda: 119.2 GiB, 128035676160 bytes, 250069680 sectors
Disk model: Generic         
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 4096 bytes
I/O size (minimum/optimal): 4096 bytes / 4096 bytes
Disklabel type: gpt
Disk identifier: 79A7A2B8-1D19-4D31-B267-5D68D3B22D01

Device     Start       End   Sectors   Size Type
/dev/sda1   2048      4095      2048     1M BIOS boot
/dev/sda2   4096 250066943 250062848 119.2G Linux filesystem


GPT PMBR size mismatch (250069679 != 976773167) will be corrected by write.
The backup GPT table is not on the end of the device. This problem will be corrected by write.
Disk /dev/sdb: 465.8 GiB, 500107862016 bytes, 976773168 sectors
Disk model: 500G2B0A-00SM50
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 4096 bytes
I/O size (minimum/optimal): 4096 bytes / 4096 bytes
Disklabel type: gpt
Disk identifier: 79A7A2B8-1D19-4D31-B267-5D68D3B22D01

Device     Start       End   Sectors   Size Type
/dev/sdb1   2048      4095      2048     1M BIOS boot
/dev/sdb2   4096 250066943 250062848 119.2G Linux filesystem
```
パーティションを見る限り大丈夫そうです。GPTのPMBRのサイズが違うぞといわれますが、そりゃサイズが違うので警告が出ます。これは移行先SSDから起動時に修正するため無視します。

## 移行その2 - 移行先のSSDでの起動確認とパーティション拡張

次は移行先のSSDをサーバに入れて起動確認をします。この際デスクトップPCとかでディスクが複数刺さるような環境の場合は、古いSSDなりを接続しないで起動してください。UUIDが被ります。

移行先SSDをサーバに搭載後、念のためパーティションを確認します。
``` sh
fdisk -l
GPT PMBR size mismatch (250069679 != 976773167) will be corrected by write.
The backup GPT table is not on the end of the device. This problem will be corrected by write.
Disk /dev/sda: 465.8 GiB, 500107862016 bytes, 976773168 sectors
Disk model: 500G2B0A-00SM50
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 4096 bytes
I/O size (minimum/optimal): 4096 bytes / 4096 bytes
Disklabel type: gpt
Disk identifier: 79A7A2B8-1D19-4D31-B267-5D68D3B22D01

Device     Start       End   Sectors   Size Type
/dev/sda1   2048      4095      2048     1M BIOS boot
/dev/sda2   4096 250066943 250062848 119.2G Linux filesystem
```

ちゃんと認識されていますね。  
ディスクを確認する前にGPTのエラー（GPTテーブル情報の不一致）を解消します。`OK`、`Fix`と入力
``` sh
sudo parted -l
Error: The backup GPT table is corrupt, but the primary appears OK, so that will
be used.
OK/Cancel? OK                                                             
Warning: Not all of the space available to /dev/sda appears to be used, you can fix the GPT to use all of the space (an
extra 726703488 blocks) or continue with the current setting?
Fix/Ignore? Fix                                                           
Model: ATA WDC WDS500G2B0A (scsi)
Disk /dev/sda: 500GB
Sector size (logical/physical): 512B/512B
Partition Table: gpt
Disk Flags:

Number  Start   End     Size    File system  Name  Flags
 1      1049kB  2097kB  1049kB                     bios_grub
 2      2097kB  128GB   128GB   ext4
```
念のため再起動
``` sh
sudo reboot
```
fdiskでパーティション情報を確認します。
``` sh
fdisk -l
Disk /dev/sda: 465.8 GiB, 500107862016 bytes, 976773168 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: gpt
Disk identifier: 79A7A2B8-1D19-4D31-B267-5D68D3B22D01

Device     Start       End   Sectors   Size Type
/dev/sda1   2048      4095      2048     1M BIOS boot
/dev/sda2   4096 250066943 250062848 119.2G Linux filesystem
```
エラーも消えています。大丈夫そうです。
続いてfdiskでパーティションを拡張します。  
一度パーティションを削除して作成し直します。fdiskで間違って書き込まないように頻繁に`p`でパーティションを確認しながら変更。途中で間違って`w`を入力して終了しない限り破棄すればやり直せます。  
`p`でパーティション情報を確認、`d`でパーティション情報の削除、`n`でパーティションを作成、`w`でパーティション情報の書き込みます。  
パーティション情報はブートローダとシステムパーティションのみですので、基本的にパーティションナンバーは2番を操作することになります。またシステムで全領域を使用する場合はfdiskが適当なセクタサイズを計算してくれるので、基本的にEnterを押せば大丈夫です。
``` sh
sudo fdisk /dev/sda

Welcome to fdisk (util-linux 2.31.1).
Changes will remain in memory only, until you decide to write them.
Be careful before using the write command.


Command (m for help): p
Disk /dev/sda: 465.8 GiB, 500107862016 bytes, 976773168 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: gpt
Disk identifier: 79A7A2B8-1D19-4D31-B267-5D68D3B22D01

Device     Start       End   Sectors   Size Type
/dev/sda1   2048      4095      2048     1M BIOS boot
/dev/sda2   4096 250066943 250062848 119.2G Linux filesystem

Command (m for help): d
Partition number (1,2, default 2): 2

Partition 2 has been deleted.

Command (m for help): p
Disk /dev/sda: 465.8 GiB, 500107862016 bytes, 976773168 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: gpt
Disk identifier: 79A7A2B8-1D19-4D31-B267-5D68D3B22D01

Device     Start   End Sectors Size Type
/dev/sda1   2048  4095    2048   1M BIOS boot

Command (m for help): n
Partition number (2-128, default 2): 2
First sector (4096-976773134, default 4096):
Last sector, +sectors or +size{K,M,G,T,P} (4096-976773134, default 976773134):

Created a new partition 2 of type 'Linux filesystem' and of size 465.8 GiB.
Partition #2 contains a ext4 signature.

Do you want to remove the signature? [Y]es/[N]o: Y

The signature will be removed by a write command.

Command (m for help): w
The partition table has been altered.
Syncing disks.
```
続いて`resize2fs`を実行し、ファイルシステムを拡張します。

``` sh
sudo resize2fs /dev/sda2
Filesystem at /dev/sda2 is mounted on /; on-line resizing required
old_desc_blocks = 15, new_desc_blocks = 59
The filesystem on /dev/sda2 is now 122096129 (4k) blocks long.
```

これで無事パーティションの拡張が完了しているはずです。`fdisk`と`df`で確認します。
``` sh
fdisk -l
Disk /dev/sda: 465.8 GiB, 500107862016 bytes, 976773168 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: gpt
Disk identifier: 79A7A2B8-1D19-4D31-B267-5D68D3B22D01

Device     Start       End   Sectors   Size Type
/dev/sda1   2048      4095      2048     1M BIOS boot
/dev/sda2   4096 976773134 976769039 465.8G Linux filesystem
```
``` sh
df -h
Filesystem             Size  Used Avail Use% Mounted on
udev                   1.8G     0  1.8G   0% /dev
tmpfs                  375M  1.4M  374M   1% /run
/dev/sda2              458G   69G  371G  16% /
tmpfs                  1.9G  2.6M  1.9G   1% /dev/shm
tmpfs                  5.0M     0  5.0M   0% /run/lock
tmpfs                  1.9G     0  1.9G   0% /sys/fs/cgroup
tmpfs                  375M     0  375M   0% /run/user/1000
```
ちゃんと拡張されているようです。
無事これですべての作業か完了しました。

## 一通り作業を終えて

コマンドさえ間違えて入力しなければ、これでだいたいのLinuxシステムの拡張はうまくいくはずです。単純な元々のSSDのパーティション拡張することはありますが、SSDを移行を行って拡張する作業はあまりしないので、毎回びくびくしながらやっていますが・・・  
ところで`parted`コマンドを実行した後に物理セクタサイズが４Kから512になったのはなんでしょうか・・・`parted`実行時にセクタサイズを指定しなければいけなかった気がします。詳しいかたいらっしゃったら教えてくださると助かります。  
自分用のメモを兼ねていますが、コマンドラインのみでLinuxのディスク移行を行おうと考えている方の参考になれば幸いです。

## さいごに - PX128のS.M.A.R.T.情報

取り外したついでにSSDのS.M.A.R.T.情報を見てみました。NANDの予備領域を30%ほど使っているため健康状態が70%になっていますが、まだまだ何かに流用できそうです。  
![PX128のS.M.A.R.T.情報](/img/blog/20200119_px128m3_smart.png)
