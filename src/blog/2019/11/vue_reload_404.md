---
title: Vue Routerでリロード/直リンクでの404を回避する
lang: ja-JP
layout: Post
published: 2019-11-22
tags:
  - Vue
  - Netlify
category:
meta:
  - property: og:title
    content: Vue Routerでリロード/直リンクでの404を回避する
  - name: description
    content: Vue Routerでリロード/直リンクでの404を回避する
  - property: og:description
    content: Vue Routerでリロード/直リンクでの404を回避する
  - name: keywords
    content: Vue, Netlify
  - property: og:type
    content: article
---

# Vue Routerでリロード/直リンクでの404を回避する

## 症状

個人的なツールを作成したときに、Vue Routerでhistory modeを有効でサイトをNetlifyリリースしたら、リロードや直リンを貼った場合にstatus 404を返される。  
- serve(develop環境)で実行した場合は発生しない
- buildでできたページにアクセスしたときのみに発生

## 対策方法

ちょっとだけ調べると[bripkens/connect-history-api-fallback](https://github.com/bripkens/connect-history-api-fallback)を入れろやら出てくるが、そんな事をしなくて良い。  
プロジェクト直下にあるpublicディレクトリに`_redirects`ファイルを作成して、下の一行を追加するだけで良い。
```
/*    /index.html   200
```
これだけで直リンやリロードによるstatus404を回避できる。  
Netlifyでデプロイしたサイトで確認済み。
serveでうまくいっていたのにいざNetlifyにデプロイしたら404吐いてびっくりした。
