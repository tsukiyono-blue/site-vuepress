---
title: About this site
lang: ja-JP
description: tsukiyono.blueについて
meta:
  - property: og:title
    content: About this site
  - name: description
    content: tsukiyono.blueについて
  - property: og:description
    content: tsukiyono.blueについて
  - property: og:type
    content: article

---

# About

## このドメインについて

- tukiyono.blueは月夜野萌が管理するドメインです。

### このサイトについて
- サイトは[VuePress](https://vuepress.vuejs.org/)にて構築しています。
- ページのソースコードは[gitlab](https://gitlab.com/tsukiyono-blue/site-vuepress)で公開しています。
- サイトのホスティングは[Netlify](https://www.netlify.com/)で行っています。
- CDN、DNSは[Cloudflare](https://www.cloudflare.com/ja-jp/)を使用しています。

## 私のことについて
[私のことについてはこちら](./me)

