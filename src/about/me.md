---
title: About me
lang: ja-JP
description: 月夜野萌について
meta:
  - property: og:title
    content: About me
  - name: description
    content: 月夜野萌について
  - property: og:description
    content: 月夜野萌について
  - property: og:type
    content: article
---

# About Me

## プロフィール

- 月夜野萌(つきよのめぐみ)
- 由来は月夜野という地名の響きが好きだけだったりする
- 萌（もえ）と「めぐみ」をかけた。

## SNSとか

- [Mastodon](https://mstdn.tsukiyono.blue/@tsukiyonomegumi) だいたいいる
- [Twitter](https://twitter.com/tsukiyonomegumi) ほとんど絵のRTしかしてない
- [BOOK P&R](https://tsukiyonomegumi.gitlab.io/book_pr/) 読書感想文を置くところ
- [gitlab](https://gitlab.com/tsukiyonomegumi) コード置くところ
- [Keybase](https://keybase.io/tsukiyonomegumi) 本人認証だよ
