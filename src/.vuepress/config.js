module.exports = {
  title: 'tsukiyono.blue',
  description: '月夜野萌のサイト',
  dest: 'public',
  base: '/',
  theme: 'thindark',
  extendMarkdown: md => {
    md.use(require('markdown-it-fontawesome'))
  },
  plugins: {
    'sitemap': {
      hostname: 'https://www.tsukiyono.blue'
    },
    'reading-progress': {
    },
    '@vuepress/last-updated': {
    },
  },
  header: {
  },
  head: [
    ['link', { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Kosugi+Maru&display=swap&subset=japanese' }],
    ['link', { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Libre+Franklin:600&display=swap' }],
    ['link', { href: 'https://use.fontawesome.com/releases/v5.11.1/css/all.css', rel: 'stylesheet' }],
    ['link', { href: '/css/style.css', rel: 'stylesheet'}],
    ['meta', { property: 'og:site_name', content: 'tsukiyono.blue' }],
    ['meta', { name: 'twitter:card', content: 'summary' }],
    ['meta', { name: 'twitter:site', content: '@tsukiyonomegumi' }],
    ['meta', { name: 'twitter:creator', content: '@tsukiyonomegumi' }],
    
  ],
  themeConfig: {
    search: false,
    sidebar: 'auto',
    sidebarDepth: 3,
    activeHeaderLinks: true,
    pageGroup: 5,
    postTime: {
      createTime: '作成日',
      lastUpdated: '最終更新'
    },
    nav: [
      { text: 'Home', link: '/' },
      { text: 'Blog', link: '/blog/' },
      { text: 'About',
        items: [
          { text: 'This site', link: '/about/site' },
          { text: 'Me', link: '/about/me' },
        ],
      },
    ],
  }
}
