---
title: tsukiyono.blue
description: 月夜野萌のサイト
lang: ja-JP
home: true
heroImage: /img/index-avatar.png
actionText: About tsukiyono.blue
actionLink: /about/site
footer: Copyright © 2019 - Tsukiyono Megumi (月夜野萌)
meta:
  - property: og:title
    content: tsukiyono.blue
  - name: description
    content: 月夜野萌のサイト
  - property: og:description
    content: 月夜野萌のサイト
  - property: og:type
    content: website
---

<div class="features">
  <div class="feature">
    <h2><i class="fas fa-book"></i> にっき</h2>
    <p>
      <a href="/blog/"><i class="fas fa-pen-fancy"></i> にっき</a> 普段のこととか<br>
      <a href="https://tsukiyonomegumi.gitlab.io/book_pr/"><i class="fas fa-paper-plane"></i> BOOK P&R</a> 読書感想文<br>
   </p>
  </div>
  <div class="feature">
    <h2><i class="fas fa-hashtag"></i> Social Network</h2>
    <p>
      <a href="https://twitter.com/tsukiyonomegumi" target="_blank" rel="noopener noreferrer"><i class="fab fa-twitter"></i>  Twitter</a>  RTばっかり<br>
      <a rel="me" href="https://mstdn.tsukiyono.blue/@tsukiyonomegumi" target="_blank" rel="noopener noreferrer"><i class="fab fa-mastodon"></i>  Mastodon</a> 主にいるところ<br>
    </p>
  </div>
  <div class="feature">
    <h2><i class="fas fa-question-circle"></i> About Me</h2>
    <p>詳しくは<a href="/about/me">こちら</a></p>
  </div>
</div>
